# Claudia First Project

## Name
Testing DemoShopApp

## Description
This application is responsible for testing the main functionalities of the Demo Shop app.


- Login

- Logout

- Search by Product

- Add product to wishlist

- Add product to Cart

- Checkout

## Software Engineer: Claudia Vieru


## Technologies used:


- IntelliJ IDEA Community Edition

- Java version 17.0.5

- Apache Maven 2.22.2

- TestNG Version 7.7.1

- Selenide version 6.13.0

- Allure version 2.21.0

- Browser Chrome Version 112.0.5615.50 (Official Build) (64-bit)

## How to run tests

git clone https://gitlab.com/claudia.vieru/claudia-first-project.git
Execute the following commands to:

## Execute all tests

- mvn clean tests

## Generate Report

- mvn allure:report

Open and present report

- mvn allure:serve


