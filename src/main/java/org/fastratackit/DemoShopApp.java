package org.fastratackit;

import org.fastratackit.body.Header;
import org.fastratackit.body.Modal;
import org.fastratackit.pages.MainPage;
import org.fastratackit.pages.WishlistMainPage;

public class DemoShopApp {

    private static final String APP_TITLE = "Demo Shop Testing simulator";
    public static final String DEMO_SHOP_TITLE = "Demo shop";

    public static void main(String[] args) {

        System.out.println(APP_TITLE);
        MainPage homePage = new MainPage();
        verifyStaticPage(homePage);

        verifyLoginModal(homePage);

        verifyWishlistPage(homePage);

        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clickOnUsernameField();
        String beetleUser = "beetle";
        modal.fillInUsernameField(beetleUser);
        modal.clickOnPasswordField();
        modal.fillInPasswordField("choochoo");
        modal.clickOnSubmitButton();

        Header loggedInHeader = new Header(beetleUser);
        homePage.setHeader(loggedInHeader);
        homePage.validateThatHeaderContainsAllElements();
    }

    private static void verifyWishlistPage(MainPage homePage) {
        homePage.clickOnTheWishlistButton();
        WishlistMainPage wishlistMainPage = new WishlistMainPage();
        wishlistMainPage.validateThatWishlistPageIsDisplayed();
        wishlistMainPage.clickOnTheLogoButton();
    }

    private static Modal verifyLoginModal(MainPage homePage) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.validateModalComponents();
        modal.clickOnCloseButton();
        homePage.validateModalIsNotDisplayed();
        return modal;
    }

    private static void verifyStaticPage(MainPage homePage) {
        homePage.getPageTitle();
        homePage.verifyThatFooterContainsAllElements();
        homePage.validateThatHeaderContainsAllElements();
    }

}