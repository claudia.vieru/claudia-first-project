package org.fastratackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fastratackit.body.Footer;
import org.fastratackit.body.Header;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;


public class MainPage extends Page{
    private final String title = Selenide.title();
    private final Footer footer;
    private Header header;

    private final SelenideElement modal = $(".modal-dialog");

    private final SelenideElement searchField = $("#input-search");

    private final SelenideElement searchButton = $(".btn-light");
    private final SelenideElement card = $(".card-link");


    public MainPage() {
        System.out.println("Constructing Footer");
        this.footer = new Footer();
        System.out.println("COnstructing Header");
        this.header = new Header();
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Footer getFooter() {
        return footer;
    }

    public Header getHeader() {
        return header;
    }



    public String getPageTitle() {
        return title;

    }

    public void verifyThatFooterContainsAllElements() {
        System.out.println("Verify footer details are: " + footer.getDetails());
        System.out.println("Verify footer has Question icon " + footer.getQuestionIcon());
        System.out.println("Verify footer has Refresh icon: " + footer.getResetIconTitle());
    }

    public void validateThatHeaderContainsAllElements() {
        System.out.println("Verify that logo url is: " + header.getLogoIconUrl());
        System.out.println("Verify that shopping cart url is: " + header.getShoppingCartIconUrl());
        System.out.println("Verify that wishlist cart url is: " + header.getWishlistIconUrl());
        System.out.println("Verify that greeting message is: " + header.getGreetingMessage());
        System.out.println("Verify that sign in is: " + header.getSignInButton());
    }

    public void clickOnTheLoginButton() {
        this.header.clickOnTheLoginButton();
        sleep(300);
    }

    public boolean validateModalIsDisplayed(){
        System.out.println("Verify that the modal is displayed on page");
        return this.modal.exists() && this.modal.isDisplayed();

    }
    public boolean validateModalIsNotDisplayed() {
        System.out.println("Verify that Modal is not displayed");
        return modal.exists();
    }

    public void clickOnTheWishlistButton() {
        this.header.clickOnTheWishlistIcon();
    }

    public void clickOnTheLogoButton(){
        this.header.clickOnTheLogoIcon();
    }

    public void logUserOut() {
        this.header.clickOnTheLogoutButton();
    }

    public void clickOnSearchFieldAndFillIn(String search) {
        this.searchField.click();
        this.searchField.sendKeys(search);
    }

    public void clickOnSearchButton() {
        this.searchButton.click();
    }

    public String validateSearchReturnsProductsNames() {
      return this.card.getText();
    }
}
