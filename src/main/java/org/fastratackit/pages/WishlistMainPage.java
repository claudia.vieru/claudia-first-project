package org.fastratackit.pages;

public class WishlistMainPage extends MainPage{

    private final String pageSubtitle;

    public WishlistMainPage() {
        this.pageSubtitle = "Wishlist";
        System.out.println("Wishlist page was created!");
    }

    public void validateThatWishlistPageIsDisplayed() {
        System.out.println("Validate that Wishlist Page is Displayed");
    }
}
