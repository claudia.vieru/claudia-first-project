package org.fastratackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Modal {
    private final SelenideElement modalTitle = $(".modal-title");
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".btn-primary");

    private final SelenideElement incorrectUsernameOrPassword = $(".fa-exclamation-circle");

    public Modal() {
    }

    public String getModalTitle() {

        return modalTitle.text();
    }

    public void clickOnCloseButton() {

        System.out.println("Clicked on the 'x' button");
        this.closeButton.click();


    }

    public void validateModalComponents() {
        System.out.println("Verify that Modal close button is " + closeButton);
        System.out.println("Verify that username field is displayed");
        System.out.println("Verify that password field is displayed");
        System.out.println("Verify that Login button is displayed");
        System.out.println("Verify that Login button is enabled");
    }


    public void clickOnUsernameField() {

        this.username.click();
    }

    public void fillInUsernameField(String userToType) {
        this.username.click();
        this.username.sendKeys(userToType);
    }

    public void clickOnPasswordField() {
        System.out.println("Clicked on " + this.password);
    }

    public void fillInPasswordField(String passwordToType) {
        System.out.println("Typed in password: " + passwordToType);
        this.password.click();
        this.password.sendKeys(passwordToType);
    }

    public void clickOnSubmitButton() {
        sleep(3000);
        System.out.println("Clicked on the Submit button");
        this.loginButton.click();
    }
    public boolean validateIncorrectUsernameOrPasswordMessageIsDisplayed(){
        return this.incorrectUsernameOrPassword.isDisplayed();
    }


    public boolean validateCloseButtonIsDisplayed() {
        return this.closeButton.exists() && this.closeButton.isDisplayed();
    }

    public boolean validateUsernameFieldIsDisplayed() {
        return this.username.exists() && this.username.isDisplayed();
    }
}
