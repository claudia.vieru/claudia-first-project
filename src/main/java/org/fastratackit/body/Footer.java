package org.fastratackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement details = $(".nav-link");
    private final SelenideElement questionIcon = $(".fa-question");

    private final SelenideElement resetIconTitle = $(byXpath("//button[@title='Reset the application state']"));

    public String getDetails() {
        return this.details.getText();
    }

    public SelenideElement getQuestionIcon() {
        return questionIcon;
    }

    public SelenideElement getResetIconTitle() {
        return resetIconTitle;
    }
}
