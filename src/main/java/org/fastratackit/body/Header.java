package org.fastratackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Header {
    private final String logoIconUrl;

    private final String shoppingCartIcon;
    
    private final String wishlist;
    
    private final String greetingsMessage;

    private final SelenideElement signInButton = $(".fa-sign-in-alt");
    private final SelenideElement signOutButton = $(".fa-sign-out-alt");
    private final SelenideElement mandatoryUsernameValidationMessage = $(".error");

    private final SelenideElement logoImage = $(".fa-shopping-bag");

    public Header() {
        this.logoIconUrl = "/";
        this.shoppingCartIcon = "/cart";
        this.wishlist = "/wishlist";
        this.greetingsMessage = "Hello guest!";
    }

    public Header(String user){
        this.logoIconUrl = "/";
        this.shoppingCartIcon = "/cart";
        this.wishlist = "/wishlist";
        this.greetingsMessage = "Hi " + user + "!";
    }

    public String getLogoIconUrl() {
        return logoIconUrl;
    }

    public String getShoppingCartIconUrl() {
        return shoppingCartIcon;
    }
    public String getWishlistIconUrl() {
        return wishlist;
    }


    public String getGreetingMessage() {
        return greetingsMessage;
    }

    public SelenideElement getSignInButton() {
        return signInButton;
    }

    public void clickOnTheLoginButton(){
        System.out.println("Clicked on the Login Button.");
        this.signInButton.click();
        sleep(100);
    }

    public void clickOnTheLogoutButton(){
        this.signOutButton.click();
        sleep(100);
    }

    public boolean isUsernameErrorMessageIsDisplayed(){
        return this.mandatoryUsernameValidationMessage.isDisplayed();
    }

    public void clickOnTheWishlistIcon() {
        System.out.println("Clicked on the Wishlist Button");
    }
    public void clickOnTheLogoIcon(){
        this.logoImage.click();
        System.out.println("Return to Home page");
    }
}
