package org.fastratackit.body.products;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final SelenideElement productLink;
    public final SelenideElement card;

    private final SelenideElement addToBasketButton;

    private final SelenideElement addToFavorites;
    private final SelenideElement productImage = $(".product-image");

    private final SelenideElement productButton = $(".card-body [href='#/product/5");
    private final SelenideElement productAddedToBasket = $(".fa-layers-counter"); //".shopping-cart-icon .fa-layers~span"

    private final SelenideElement productAddedToFavorites = $(".fa-heart"); //".shopping-cart-icon .fa-heart~span" - heart nr
    private final SelenideElement goToShoppingCartButton = $(".fa-shopping-cart");

    private final SelenideElement emptyCardMessage = $(byText("How about adding some products in your cart?"));

    private final SelenideElement continueShoppingButton = $(byText("Continue Shopping"));

    private final SelenideElement deleteProductFromBasketButton = $(".fa-trash");

    private final SelenideElement productsText = $(byText("Products"));

    private final SelenideElement yourInformationText = $(byText("Your information"));

    private final SelenideElement checkoutButton = $(byText("Checkout"));

    private final SelenideElement firstNameRequiredErrorMessage = $(byText("First Name is required"));

    private final SelenideElement continueCheckoutButton = $(".fa-angle-right");

    private final SelenideElement firstNameFieldCheckoutInfo = $("#first-name");

    private final SelenideElement lastNameRequiredErrorMessage = $(byText("Last Name is required"));

    private final SelenideElement lastNameFieldCheckoutInfo = $("#last-name");

    private final SelenideElement addressRequiredErrorMessage = $(byText("Address is required"));

    private final SelenideElement orderSummaryMessage = $(byText("Order summary"));

    private final SelenideElement addressFieldCheckoutInfo = $("#address");

    private final SelenideElement completeYourOrder = $(byText("Complete your order"));

    private final SelenideElement finalisedOrderWithSuccess = $(byText("Thank you for your order!"));

    public Product(String productId) {
        this.productLink = $(String.format(".card-body [href='#/product/%s'", productId));
        this.card = productLink.parent().parent();
        this.addToBasketButton = card.$(".fa-cart-plus");
        this.addToFavorites = card.$(".fa-heart");

    }

    public void clickOnProduct() {

        this.productLink.click();
    }

    public void addProductToBasket() {
        this.addToBasketButton.click();

    }

    public void addToFavorites() {

        this.addToFavorites.click();
    }

    public boolean validateProductImageIsDisplayed() {

        return this.productImage.isDisplayed();
    }

    public void selectProduct() {

        this.productButton.click();
    }

    public boolean productIsAddedToBasket(String value) {
        return this.productAddedToBasket.getText().equals(value) &&
                this.productAddedToBasket.isDisplayed();
    }

    public String getProductName(){
        return this.productLink.getText();
    }

    public boolean productIsAddedToFavorites()
    {
        return this.productAddedToFavorites.exists()&&
                this.productAddedToFavorites.isDisplayed();
    }
    public boolean yourCartIsEmptyMessageIsDisplayed(){
        return this.emptyCardMessage.exists() &&
                this.emptyCardMessage.isDisplayed();
    }

    public void goToShoppingCart(){

        this.goToShoppingCartButton.click();
    }
    public boolean iSDeletedProductFromBasket() {
        return  this.emptyCardMessage.exists() &&
              this.emptyCardMessage.isDisplayed();
    }

    public void deleteProductFromBasket(){

        this.deleteProductFromBasketButton.click();
    }

    public void continueShopping(){
        this.continueShoppingButton.click();
    }

    public boolean productsTextIsDisplayed(){
        return this.productsText.exists() &&
                this.productsText.isDisplayed();
    }
    public void clickCheckoutButton(){

        this.checkoutButton.click();
    }
    public boolean yourInformationPageIsDisplayed(){
        return this.yourInformationText.exists() &&
                this.yourInformationText.isDisplayed();
    }
    public void clickContinueCheckoutButton(){
        this.continueCheckoutButton.click();
    }
    public boolean firstNameIsRequiredErrorMessage(){
        return this.firstNameRequiredErrorMessage.exists() &&
                this.firstNameRequiredErrorMessage.isDisplayed();
    }
    public void fillInFirstNameFieldCheckoutInfo(){
        this.firstNameFieldCheckoutInfo.sendKeys("Claudia");
    }

    public boolean lastNameIsRequiredErrorMessage() {
        return this.lastNameRequiredErrorMessage.exists() &&
                this.lastNameRequiredErrorMessage.isDisplayed();
    }
    public void fillInLastNameFieldCheckoutInfo(){
        this.lastNameFieldCheckoutInfo.sendKeys("Vieru");
    }

    public boolean addressIsRequiredErrorMessage(){
        return this.addressRequiredErrorMessage.exists() &&
                this.addressRequiredErrorMessage.isDisplayed();
    }

    public void fillInAddressFieldCheckoutInfo(){
        this.addressFieldCheckoutInfo.sendKeys("Aluviunii 47");
    }
    public boolean orderSummaryPageIsDisplayed(){
        return this.orderSummaryMessage.exists() &&
                this.orderSummaryMessage.isDisplayed();
    }
    public void clickCompleteYourOrderButton()
    {
        this.completeYourOrder.click();
    }

    public boolean orderCompletePageIsDisplayed(){
        return this.finalisedOrderWithSuccess.exists() &&
                this.finalisedOrderWithSuccess.isDisplayed();
    }
}
