package org.fasttrackit;

import org.fastratackit.body.Header;
import org.fastratackit.body.products.Product;
import org.fastratackit.pages.MainPage;
import org.testng.annotations.*;
import org.fastratackit.body.Modal;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.*;

public class LoginTest {
    MainPage homePage = new MainPage();

    @BeforeClass
    public void openBrowserBeforeClass() {
        open("https://fasttrackit-test.netlify.app/#/");
    }

    @AfterClass
    public void tearDownAfterClass() {
        closeWebDriver();
    }

    @BeforeMethod
    public void openBrowser() {
        open("https://fasttrackit-test.netlify.app/#/");
    }

    @AfterMethod
    public void tearDown() {
        closeWebDriver();
    }

    @Test
    public void modalComponentsAreDisplayedAndModalCanBeClosedTest() {
        homePage.clickOnTheLoginButton();
        boolean modalIsDisplayed = homePage.validateModalIsDisplayed();
        assertTrue(modalIsDisplayed, "Expected modal is displayed");
        Modal modal = new Modal();
        assertEquals(modal.getModalTitle(), "Login", "Expected Modal title to be Login");
        assertTrue(modal.validateCloseButtonIsDisplayed(), "Expected Close button to be displayed");
        assertTrue(modal.validateUsernameFieldIsDisplayed(), "Expected Username field to be displayed");
        modal.validateModalComponents();
        modal.clickOnCloseButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected Modal to be closed");
    }

    @Test
    public void userCanLoginOnPageTest() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "beetle";
        modal.fillInUsernameField(user);
        modal.fillInPasswordField("choochoo");
        modal.clickOnSubmitButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed");
        Header header = new Header(user);
        assertEquals(header.getGreetingMessage(), "Hi beetle!", "Expected greetings message to contain Hi beetle");
        homePage.logUserOut();
    }

    @Test
    public void userCantLoginOnPageWithoutPasswordTest() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "beetle";
        modal.fillInUsernameField(user);
        modal.clickOnSubmitButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertTrue(modal.validateIncorrectUsernameOrPasswordMessageIsDisplayed(), "Incorrect username/password error message is displayed");
        modal.clickOnCloseButton();

    }

    @Test
    public void validationMessageMandatoryUsernameFieldTest() {
        homePage.clickOnTheLoginButton();
        sleep(3000);
        Modal modal = new Modal();
        modal.clickOnSubmitButton();
        assertTrue(modal.validateIncorrectUsernameOrPasswordMessageIsDisplayed(), "Please fill in the username! message is displayed");


    }

    @Test
    public void searchProductTest() {
        String search = "Practical";
        homePage.clickOnSearchFieldAndFillIn(search);
        homePage.clickOnSearchButton();
        sleep(5000);
        assertTrue(homePage.validateSearchReturnsProductsNames().contains(search), "Product name exists on page.");
    }

    @Test
    public void validationMessageIncorrectUsernameOrPassword() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "incorrect";
        modal.fillInUsernameField(user);
        modal.fillInPasswordField("pass1234");
        modal.clickOnSubmitButton();
        assertTrue(modal.validateIncorrectUsernameOrPasswordMessageIsDisplayed(), " Incorrect username or password!");
    }

    @Test
    public void validateProductPageIsDisplayed() {
        Product product = new Product("5");
        product.selectProduct();
        assertTrue(product.validateProductImageIsDisplayed(), "Product page is displayed");
    }
}
