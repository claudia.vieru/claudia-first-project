package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.fastratackit.pages.MainPage;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.*;


public class StaticApplicationStateTest {

    MainPage homePage = new MainPage();

    @BeforeClass
    public void openBrowserBeforeClass() {
        open("https://fasttrackit-test.netlify.app/#/");
    }

    @AfterClass
    public void tearDownAfterClass() {
        closeWebDriver();
    }

    @BeforeMethod
    public void openBrowser() {
        open("https://fasttrackit-test.netlify.app/#/");
    }

    @AfterMethod
    public void tearDown() {
        closeWebDriver();
    }


    @Test
    public void verifyDemoShopAppTitle() {
        String appTitle = homePage.getPageTitle();
        assertEquals(appTitle, "Demo shop", "Application title is expected to be Demo shop");
        sleep(5000);

    }

    @Test
    public void verifyDemoShopFooterBuildDateDetails() {
        String footerDetails = homePage.getFooter().getDetails();
        assertEquals(footerDetails, "Demo Shop | build date 2021-05-21 14:04:30 GTBDT", "Expected footer details to be: Demo Shop | build date 2021-05-21 14:04:30 GTBDT");

    }

    @Test
    public void verifyDemoShopFooterContainsQuestionIcon() {
        SelenideElement questionIcon = homePage.getFooter().getQuestionIcon();
        assertTrue(questionIcon.exists(), "Expected Question icon to exist on Page");
        assertTrue(questionIcon.isDisplayed(), "Expected Question icon to be displayed");


    }

    @Test
    public void verifyDemoShopFooterContainsResetIcon() {
        SelenideElement resetIconTitle = homePage.getFooter().getResetIconTitle();
        assertTrue(resetIconTitle.exists(), "Expected Reset icon to exist on Page");
        assertTrue(resetIconTitle.isDisplayed(), "Expected Reset icon to be displayed");
    }
}
