package org.fasttrackit;

import org.fastratackit.body.products.Product;
import org.fastratackit.pages.MainPage;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.assertTrue;

public class ProductTest {
    MainPage homePage = new MainPage();
    Product product = new Product("1");

    @BeforeClass
    public void openBrowserBeforeClass() {
        open("https://fasttrackit-test.netlify.app/#/");
    }

    @AfterClass
    public void tearDownAfterClass() {
        closeWebDriver();
    }

    @BeforeMethod
    public void openBrowserBeforeMethod() {
        open("https://fasttrackit-test.netlify.app/#/");
    }

    @AfterMethod
    public void tearDownAfterMethod() {
        closeWebDriver();
    }

    @Test
    public void userCanAddProductToBasket() {
        product.addProductToBasket();
        sleep(3000);
        assertTrue(product.productIsAddedToBasket("1"), "Product was added to Basket");
        product.addProductToBasket();
        sleep(3000);
        assertTrue(product.productIsAddedToBasket("2"), "Product was added to Basket");
    }

    @Test
    public void userCanDeleteAddedProductFromBasket() {
        product.addProductToBasket();
        product.goToShoppingCart();
        product.deleteProductFromBasket();
        assertTrue(product.iSDeletedProductFromBasket(), "Product was deleted from Basket");
    }

    @Test
    public void continueShoppingButtonRedirectsUserBackToProductsPage() {
        product.addProductToBasket();
        product.goToShoppingCart();
        product.continueShopping();
        assertTrue(product.productsTextIsDisplayed(), "Continue shopping redirects to Products page");
    }

    @Test
    public void checkOutButtonRedirectsToYourInformationPage() {
        product.addProductToBasket();
        product.goToShoppingCart();
        product.clickCheckoutButton();
        assertTrue(product.yourInformationPageIsDisplayed(), "Checkout redirects to Your information page");
    }

    @Test
    public void firstNameRequiredCheckoutInfo() {
        product.addProductToBasket();
        product.goToShoppingCart();
        product.clickCheckoutButton();
        product.clickContinueCheckoutButton();
        sleep(3000);
        assertTrue(product.firstNameIsRequiredErrorMessage(), "Validation error for First Name is required is displayed");
    }

    @Test
    public void lastNameRequiredCheckoutInfo() {
        product.addProductToBasket();
        product.goToShoppingCart();
        product.clickCheckoutButton();
        product.fillInFirstNameFieldCheckoutInfo();
        product.clickContinueCheckoutButton();
        sleep(3000);
        assertTrue(product.lastNameIsRequiredErrorMessage(), "Validation error for Last Name is required is displayed");
    }

    @Test
    public void addressRequiredCheckoutInfo() {
        product.addProductToBasket();
        product.goToShoppingCart();
        product.clickCheckoutButton();
        product.fillInFirstNameFieldCheckoutInfo();
        sleep(2000);
        product.fillInLastNameFieldCheckoutInfo();
        product.clickContinueCheckoutButton();
        assertTrue(product.addressIsRequiredErrorMessage(), "Validation error for address is required is displaye");

    }

    @Test
    public void orderSummaryPage(){
        product.addProductToBasket();
        product.goToShoppingCart();
        product.clickCheckoutButton();
        product.fillInFirstNameFieldCheckoutInfo();
        product.fillInLastNameFieldCheckoutInfo();
        product.fillInAddressFieldCheckoutInfo();
        product.clickContinueCheckoutButton();
        assertTrue(product.orderSummaryPageIsDisplayed(), "Order Summary Page is displayed");
    }

    @Test
    public void completeYourOrder(){
        product.addProductToBasket();
        product.goToShoppingCart();
        product.clickCheckoutButton();
        product.fillInFirstNameFieldCheckoutInfo();
        product.fillInLastNameFieldCheckoutInfo();
        product.fillInAddressFieldCheckoutInfo();
        product.clickContinueCheckoutButton();
        product.clickCompleteYourOrderButton();
        assertTrue(product.orderCompletePageIsDisplayed(), "Order Complete Page is displayed");

    }

    @Test
    public void userCanAddProductToFavorites() {
        product.addToFavorites();
        assertTrue(product.productIsAddedToFavorites(), "Product was added to Favorites");
    }

    @Test
    public void yourCartIsEmptyMessageIsDisplayed() {
        product.goToShoppingCart();
        assertTrue(product.yourCartIsEmptyMessageIsDisplayed(), "Empty card message is displayed");
    }
}
